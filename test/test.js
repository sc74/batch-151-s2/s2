const { div_check, factorial } = require('../src/util.js');

const { expect, assert } = require('chai');

describe('Test Factorials', ()=> {
	it('Test that 5! is 120', () => {
		const product = factorial(5)
		expect(product).to.equal(120)
	});

	it('Test that 1! is 1', ()=>{
		const product = factorial(1)
		assert.equal(product, 1)
	});
		it('Test that 0! is 1', ()=>{
		const product = factorial(0)
		assert.equal(product, 1)
	});
		it('Test that 4! is 24', ()=>{
		const product = factorial(4)
		assert.equal(product, 24)
	});

		it('Test that 10! is 3628800', ()=>{
		const product = factorial(10)
		expect(product).to.equal(3628800)
	});
		it('Test negative factorial is undefined', () => {
			const product = factorial(-1); 
			expect(product).to.equal(undefined);
	});
		it('Test the non numeric value returns an error', () => {
			const product = factorial("false"); 
			expect(product).to.equal(undefined);
	});
});

describe('Test Div', ()=> {
	it('Test that 105 is div by 5', () => {
		const mod = div_check(105)
		expect(mod).to.equal(0)
	});
	it('Test that 14 is div by 7', () => {
		const mod = div_check(10)
		expect(mod).to.equal(0)
	});
	it('Test that 0 is div by 5 or 7', () => {
		const mod = div_check(0)
		expect(mod).to.equal(0)
	});
	it('Test that 22 is not div by 5 or 7', () => {
		const mod = div_check(22) 
		assert.notEqual(mod, 0)
	});
});

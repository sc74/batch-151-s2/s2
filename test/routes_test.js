const chai = require('chai')
const expect = chai.expect
const http = require('chai-http')

chai.use(http);

describe('API Test Suite', ()=> {
	it('Test API Get people is running', ()=>{
		chai.request('http://localhost:5001').get('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined)
		})
	})
	it('Test API get people returns 200', ()=>{
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res)=>{
			expect(res.status).to.equal(200)
		})
	})
	it('Test API post person returns 400 if no NAME property',(done)=>{
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Mikasa"
		})	
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Test API POST endpoint is running', ()=>{
		chai.request('http://localhost:5001').post('/person')
		.end((err, res) => {
			expect(res).to.not.equal(undefined)
		})
	})
	it('Test API post person returns 400 if no ALIAS', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "Ackerman",
			age: 27
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Test API post person returns 400 if no AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Mikasa",
			name: "Ackerman",
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})
});

describe ('Test Currency', ()=>{
	it('Test API POST endpoint is running', ()=>{
		chai.request('http://localhost:5001').post('/currency')
		.end((err, res) => {
			expect(res).to.not.equal(undefined)
		})
	})
	it('Test API post currency returns 400 if no NAME property',(done)=>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "test",
			ex: {
	        "peso": 0.043,
	    	}
		})	
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test API post currency returns 400 if NAME is empty string',(done)=>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "test",
			name: "test",
			ex: {
	        "peso": 0.043,
	    	}
		})	
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Test API post currency returns 400 if NAME is not a string',(done)=>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "test",
			name: false,
			ex: {
	        "peso": 0.043,
	    	}
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test API post currency returns 400 if no EX property',(done)=>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "test",
			name: "test1",
		})	
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test API post currency returns 400 if EX is not an object',(done)=>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "test",
			name: "false",
			ex: "peso"
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test API post currency returns 400 if no ALIAS property',(done)=>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "test1",
	      	ex: {
	        	"peso": 0.043,
	        }
		})	
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test API post currency returns 400 if ALIAS is not a string',(done)=>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: false,
			name: "false",
			ex: {
	        "peso": 0.043,
	    	}
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
})
const { names, exchangeRates } = require ('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res)=> {
		return res.send({data: {} })
	});

	app.get('/people', (req, res)=>{
		return res.send({
			people: names
		})
	});

	app.post('/person', (req, res) =>{
		if (!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter NAME'
			})
		}
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : NAME has to be string'
			})
		}
		if(!req.body.hasOwnProperty('age')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter AGE'
			})
		}
		if(typeof req.body.age !== 'number'){
			return res.status(400).send({
				'error' : 'Bad Request : AGE has to be a number'
			})
		}
		if(!req.body.hasOwnProperty('alias')){
				return res.status(400).send({
					'error' : 'Bad Request: Alias has to be a string'
				})
			}
	});
	app.post('/currency', (req, res) =>{
		if (!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter NAME'
			})
		}
		if (!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter EX'
			})
		}

		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : NAME has to be string'
			})
		}
		if( req.body.name === "" ){
			return res.status(400).send({
				'error' : 'Bad Request : NAME has to be filled'
		}
		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				'error' : 'Bad Request : NAME has to be an object'
			})
		}
		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter ALIAS'
			})
		}
		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : ALIAS has to be a string'
			})
		}
	})
}